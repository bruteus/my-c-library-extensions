#ifndef STRING_T_H
#define STRING_T_H

#include <stdlib.h>
#include <string.h>

/*
  Custom malloc and realloc that allocate one byte more
  than s and places '\0' at the end of allocated memory
 */
extern char *str_malloc(size_t s);
extern char *str_realloc(char *alloc, size_t s);

typedef struct {
	char *s;
	size_t len;
} string_t;

/*
  Index normalization
  If idx is negative then its value is calculated
  as (len + idx) - as we would count from the end
  of the string. Behaviour similar to python language.
 */
extern int idxnorm(int idx, size_t len);

/* Constructors */
extern string_t mkstrem();                          /* Make empty string_t */
extern string_t mkstr(const char *s);               /* Make string_t from char array of unknown length */
extern string_t mkstrl(const char *s, size_t len);  /* Make string_t from char array of known length */

/* Changing the string */
extern void chstr(string_t *dest, const char *s);               /* Change to char array of unknown length */
extern void chstrl(string_t *dest, const char *s, size_t len);  /* Change to char array of known length */
extern void chstrstr(string_t *dest, string_t src);             /* Change to another string_t value */

/* String operations */
extern void appstr(string_t *dest, const char *s);               /* Append char array of unknown length */
extern void appstrl(string_t *dest, const char *s, size_t len);  /* Append char array of known length */
extern void appstrstr(string_t *dest, string_t src);             /* Append another string_t */
extern int cmpstr(string_t str1, string_t str2);                 /* Compare two string_t objects */
extern string_t substr(string_t src, int start, int end);        /* Return substring between start and end */
extern void erasestr(string_t *str);                             /* Deallocate used memory and set len to zero */

/* Searching */
extern int locchr(string_t src, int start, char c);         /* Locate char in the string */
extern int locstr(string_t src, int start, const char *s);  /* Locate char array in the string */
extern int locstrstr(string_t src, int start, string_t s);  /* Locate another string in the string */

#include "./string_t.c"

#endif /* STRING_T_H */
