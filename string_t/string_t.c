/* Utility functions */
char *str_malloc(size_t s) {
	char *alloc = malloc(s + 1);
	alloc[s] = '\0';
	return alloc;
}

char *str_realloc(char *alloc, size_t s) {
	alloc = realloc(alloc, s + 1);
	alloc[s] = '\0';
	return alloc;
}

int idxnorm(int idx, size_t len) {
	if(idx < 0)
		idx = len + idx;
	return ((idx < 0 || idx >= len) ? -1 : idx);
}


/* Constructors */
string_t mkstrem() {
	string_t newstr = {NULL, 0};
	return newstr;
}

string_t mkstr(const char *s) {
	string_t newstr;
	newstr.len = strlen(s);
	newstr.s = str_malloc(newstr.len);
	memcpy(newstr.s, s, newstr.len);
	return newstr;
}

string_t mkstrl(const char *s, size_t len) {
	string_t newstr;
	newstr.s = str_malloc(len);
	memcpy(newstr.s, s, len);
	newstr.len = len;
	return newstr;
}


/* Changing the string */
void chstr(string_t *dest, const char *s) {
	size_t newlen = strlen(s);
	if(dest->len != newlen) {
		dest->s = str_realloc(dest->s, newlen);
		dest->len = newlen;
	}
	
	memcpy(dest->s, s, newlen);
}

void chstrl(string_t *dest, const char *s, size_t len) {
	if(dest->len != len) {
		dest->s = str_realloc(dest->s, len);
		dest->len = len;
	}

	memcpy(dest->s, s, len);
}

void chstrcpy(string_t *dest, string_t src) {
	if(dest->len != src.len) {
		dest->s = str_realloc(dest->s, src.len);
		dest->len = src.len;
	}

	memcpy(dest->s, src.s, dest->len);
}


/* String operations */
void appstr(string_t *dest, const char *s) {
	size_t applen = strlen(s);
	if(dest->len == 0) {
		free(dest->s);
		dest->s = str_malloc(applen);
	}
	else
		dest->s = str_realloc(dest->s, dest->len + applen);
	memcpy(dest->s + dest->len, s, applen);
	dest->len += applen;
}

void appstrl(string_t *dest, const char *s, size_t len) {
	if(dest->len == 0) {
		free(dest->s);
		dest->s = str_malloc(len);
	}
	else
		dest->s = str_realloc(dest->s, dest->len + len);
	memcpy(dest->s + dest->len, s, len);
	dest->len += len;
}

void appstrstr(string_t *dest, string_t src) {
	if(dest->len == 0) {
		free(dest->s);
		dest->s = str_malloc(src.len);
	}
	else
		dest->s = str_realloc(dest->s, dest->len + src.len);
	memcpy(dest->s + dest->len, src.s, src.len);
	dest->len += src.len;
}

int cmpstr(string_t str1, string_t str2) {
	if(str1.len != str2.len)
		return 0;

	if(memcmp(str1.s, str2.s, str1.len) != 0)
		return 0;

	return 1;
}

string_t substr(string_t src, int start, int end) {
	/* Return string_t type substring from this range: <start, end) */
	string_t substring = mkstrem();

	start = (start == src.len ? start : idxnorm(start, src.len));
	end = (end == src.len ? end : idxnorm(end, src.len));

	/* If arguments ar invalid return empty string_t object */
	if(start == -1 || end == -1 || start > end)
		return substring;

	substring.len = end - start;
	substring.s = str_malloc(substring.len);
	memcpy(substring.s, src.s + start, substring.len);

	return substring;
}

void erasestr(string_t *str) {
	if(str->s != NULL)
		free(str->s);
	str->len = 0;
}


/* Searching */
int locchr(string_t src, int start, char c) {
	start = idxnorm(start, src.len);
	if(start == -1)
		return -1;
	
	char *p = memchr(src.s + start, c, src.len - start);
	
	return (p == NULL ? -1 : p - src.s);
}

int locstr(string_t src, int start, const char *s) {
	start = idxnorm(start, src.len);
	if(start == -1)
		return -1;

	char *p = strstr(src.s + start, s);

	return (p == NULL ? -1 : p - src.s);
}

int locstrstr(string_t src, int start, string_t s) {
	start = idxnorm(start, src.len);
	if(start == -1)
		return -1;

	char *p = strstr(src.s + start, s.s);

	return (p == NULL ? -1 : p - src.s);
}
