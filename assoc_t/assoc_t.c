kwarg_t *kwarg_malloc(size_t s) {
	return (kwarg_t *)malloc(s * sizeof(kwarg_t));
}

kwarg_t *kwarg_realloc(kwarg_t *alloc, size_t s) {
	alloc = (kwarg_t *)realloc(alloc, s * sizeof(kwarg_t));
	return alloc;
}


/* kwarg_t constructors */
kwarg_t mkkwarg(const char *k, const char *v) {
	kwarg_t newkwarg = {mkstr(k), mkstr(v)};
	return newkwarg;
}

kwarg_t mkkwargstr(string_t k, string_t v) {
	kwarg_t newkwarg = {k, v};
	return newkwarg;
}


/* assoc_t constructors */
assoc_t mkassoc() {
	assoc_t emptyassoc = {NULL, 0};
	return emptyassoc;
}


/* Element access */
char *getkey(assoc_t ass, int idx) {
	if((idx = idxnorm(idx, ass.len)) == -1)
		return '\0';
	return ass.kwargs[idx].key.s;
}

string_t getkeystr(assoc_t ass, int idx) {
	if((idx = idxnorm(idx, ass.len)) == -1)
		return mkstrem();
	return ass.kwargs[idx].key;
}

char *getval(assoc_t ass, int idx) {
	if((idx = idxnorm(idx, ass.len)) == -1)
		return '\0';
	return ass.kwargs[idx].val.s;
}

string_t getvalstr(assoc_t ass, int idx) {
	if((idx = idxnorm(idx, ass.len)) == -1)
		return mkstrem();
	return ass.kwargs[idx].val;
}

int lockey(assoc_t ass, const char *key) {
	for(int i = 0; i < ass.len; ++i)
		if(strcmp(ass.kwargs[i].key.s, key) == 0)
			return i;
	return -1;
}

int lockeystr(assoc_t ass, string_t key) {
	for(int i = 0; i < ass.len; ++i)
		if(cmpstr(ass.kwargs[i].key, key))
			return i;
	return -1;
}

int locval(assoc_t ass, const char *val) {
	for(int i = 0; i < ass.len; ++i)
		if(strcmp(ass.kwargs[i].val.s, val) == 0)
			return i;
	return -1;
}

int locvalstr(assoc_t ass, string_t val) {
	for(int i = 0; i < ass.len; ++i)
		if(cmpstr(ass.kwargs[i].val, val))
			return i;
	return -1;
}


/* Element operations */
void appkwarg(assoc_t *ass, const char *k, const char *v) {
	ass->len += 1;
	ass->kwargs = kwarg_realloc(ass->kwargs, ass->len);
	ass->kwargs[ass->len - 1] = mkkwarg(k, v);
}

void appkwargstr(assoc_t *ass, string_t k, string_t v) {
	ass->len += 1;
	ass->kwargs = kwarg_realloc(ass->kwargs, ass->len);
	ass->kwargs[ass->len - 1] = mkkwargstr(k, v);
}

void appkwargkw(assoc_t *ass, kwarg_t kw) {
	ass->len += 1;
	ass->kwargs = kwarg_realloc(ass->kwargs, ass->len);
	ass->kwargs[ass->len - 1] = kw;
}

void rmkwarg(assoc_t *ass, int idx) {
	idx = idxnorm(idx, ass->len);  /* idxnorm from mystring.h */
	if(idx == -1 || idx == ass->len)
		return;

	/* Free allocated memory */
	erasestr(&(ass->kwargs[idx].key));
	erasestr(&(ass->kwargs[idx].val));

	if(idx != ass->len - 1) {
		size_t bytes_to_move = sizeof(kwarg_t) * (ass->len - idx - 1);
		memmove(ass->kwargs + idx, ass->kwargs + idx + 1, bytes_to_move);
	}

	ass->len -= 1;

	ass->kwargs = kwarg_realloc(ass->kwargs, ass->len);
	/* TODO: what if kwarg_realloc moves it to a new location? */
}
