#ifndef ASSOC_T_H
#define ASSOC_T_H

#include <stdlib.h>
#include <string.h>
#include "../string_t/string_t.h"

typedef struct {
	string_t key;
	string_t val;
} kwarg_t;

typedef struct {
	kwarg_t *kwargs;
	size_t  len;
} assoc_t;

/* alloc and realloc that allocate (s * sizeof(kwarg_t)) bytes */
kwarg_t *kwarg_malloc(size_t s);
kwarg_t *kwarg_realloc(kwarg_t *alloc, size_t s);

/* Constructors for kwarg_t */
kwarg_t mkkwarg(const char *k, const char *v);  /* Make kwarg from two char arrays */
kwarg_t mkkwargstr(string_t k, string_t v);     /* Make kwarg from two strings */

/* Constructor for assoc_t */
assoc_t mkassoc();  /* Construct an empty assoc object */

/* Element access */
char *getkey(assoc_t ass, int idx);
string_t getkeystr(assoc_t ass, int idx);
char *getval(assoc_t ass, int idx);
string_t getvalstr(assoc_t ass, int idx);
int locval(assoc_t ass, const char *key);
int locvalstr(assoc_t ass, string_t key);
int lockey(assoc_t ass, const char *val);
int lockeystr(assoc_t ass, string_t val);

/* Element operations */
void appkwarg(assoc_t *ass, const char *k, const char *v);  /* Construct kwarg from two char arrays and append it */
void appkwargstr(assoc_t *ass, string_t k, string_t v);     /* Construct kwarg from two strings and append it */
void appkwargkw(assoc_t *ass, kwarg_t kw);                  /* Append kwarg */
void rmkwarg(assoc_t *ass, int idx);                        /* Remove kwarg with given index */

#include "./assoc_t.c"

#endif /* ASSOC_T_H */
