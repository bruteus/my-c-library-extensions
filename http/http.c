#include "../utils/utils.h"

HTTP_req http_generic_req(const char *path) {
	HTTP_req genreq;
	memset(genreq.http_meth, '\0', HTTPMETHSIZE);
	genreq.http_path = mkstr(path);
	genreq.http_params = mkassoc();
	strcpy(genreq.http_ver, HTTPDEFVER);
	genreq.http_heads = mkassoc();
	genreq.http_body = mkstrem();

	return genreq;
}


size_t http_req_len(const HTTP_req *req) {
	/* Starting line of HTTP request */
	size_t req_len = strlen(req->http_meth) + 1  // Method + space
		+ req->http_path.len + 1  // Path + space
		+ 8 + 2;  // HTTP version + CRLF

	/* Headers inlcuding ": " and CRLF for each one */
	for(int i = 0; i < req->http_heads.len; ++i)
		req_len += getkeystr(req->http_heads, i).len + 2  // Header name + ": "
			+ getvalstr(req->http_heads, i).len + 2;  // Header value + CRLF

	req_len += 2 + req->http_body.len;  // Last CRLF and body

	return req_len;
}


char *http_req_str(const HTTP_req *req, size_t req_len) {
	if(req_len == 0)
		req_len = http_req_len(req);
	char *req_str = malloc(req_len + 1);

	int eos = sprintf(req_str, "%s %s HTTP/%s\r\n", req->http_meth, req->http_path.s, req->http_ver);
	for(int i = 0; i < req->http_heads.len; ++i)
		eos += sprintf(req_str+eos, "%s: %s\r\n", getkey(req->http_heads, i), getval(req->http_heads, i));

	eos += sprintf(req_str+eos, "\r\n%s", (req->http_body.len != 0 ? req->http_body.s : ""));
	req_str[req_len] = '\0';  // Terminate string

	return req_str;
}


assoc_t http_extract_headers(string_t http_packet) {
	assoc_t headers = mkassoc();
	int h_start = locstr(http_packet, 0, "\r\n") + 2;
	int h_end = locstr(http_packet, h_start, "\r\n");
	while(h_start != h_end && h_start != -1 && h_end != -1) {  /* Assuming that http_packet has double CRLF */
		int delim = locchr(http_packet, h_start, ':');
		appkwargstr(&headers, substr(http_packet, h_start, delim), substr(http_packet, delim+2, h_end));
		h_start = h_end + 2;
		h_end = locstr(http_packet, h_start, "\r\n");
	}

	return headers;
}


void http_recieve_res(int sockfd, HTTP_res *res) {
	char buf[BUFLEN];
	int recv_bytes = recv(sockfd, buf, BUFLEN, 0);
	if(recv <= 0)
		fatal("HTTP invalid response");

	string_t packet = mkstrl(buf, recv_bytes);

	int crlf_pos;
	/* Look for double CRLF in the packet */
	while((crlf_pos = locstr(packet, 0, "\r\n\r\n")) == -1) {
		recv_bytes = recv(sockfd, buf, BUFLEN, 0);
		if(recv_bytes <= 0)
			fatal("HTTP invalid response");

		appstrl(&packet, buf, recv_bytes);
	}

	/* Extracting data */
	strncpy(res->http_ver, packet.s + 5, 3);  /* HTTP version */
	sscanf(packet.s, "%*s %d", &(res->http_code));  /* Response code */
	res->http_codedesc = substr(packet, 13, locchr(packet, 13, '\r'));  /* Description */
	res->http_heads = http_extract_headers(packet);  /* Headers */

	/* Extracting body */
	int tmpidx;
	char *content_length = NULL;
	char *transfer_encoding = NULL;
	if((tmpidx = lockey(res->http_heads, "Content-Length")) != -1)
		content_length = getval(res->http_heads, tmpidx);
	if((tmpidx = lockey(res->http_heads, "Transfer-Encoding")) != -1)
		transfer_encoding = getval(res->http_heads, tmpidx);

	res->http_body = mkstrem();

	if(content_length != NULL) {
		int bodylen = atoi(content_length);
		int remaining = bodylen - (packet.len - (crlf_pos + 4));

		while(remaining > 0) {
			recv_bytes = recv(sockfd, buf, BUFLEN, 0);
			if(recv_bytes <= 0)
				fatal("HTTP invalid response");

			appstrl(&packet, buf, recv_bytes);
			remaining -= recv_bytes;
		}

		res->http_body = substr(packet, crlf_pos + 4, packet.len);
	}
	else if(transfer_encoding != NULL && strcmp(transfer_encoding, "chunked") == 0) {
		int chunklen;
		int curpos = crlf_pos + 4;

		while(1) {
			/* TODO add handling for chunk metadata */
			sscanf(packet.s + curpos, "%x", &chunklen);
			if(chunklen == 0)
				break;  /* TODO: add handling for chunk trailer */
			curpos = locchr(packet, curpos, '\r') + 2;

			while(chunklen > packet.len - curpos) {
				recv_bytes = recv(sockfd, buf, BUFLEN, 0);
				if(recv_bytes <= 0)
					fatal("HTTP invalid response");

				appstrl(&packet, buf, recv_bytes);
			}

			appstrl(&(res->http_body), packet.s + curpos, chunklen);
			curpos += chunklen + 2;
		}
	}

	erasestr(&packet);  /* Free allocated memory */
}
