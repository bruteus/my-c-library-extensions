#ifndef HTTP_H
#define HTTP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../string_t/string_t.h"
#include "../assoc_t/assoc_t.h"

#include <sys/socket.h>

#define BUFLEN 4096
#define HTTPMETHSIZE 5
#define HTTPVERSIZE 4
#define HTTPDEFVER "1.1"

/* HTTP request */
typedef struct {
	char     http_meth[HTTPMETHSIZE];
	string_t http_path;
	assoc_t  http_params;
	char     http_ver[HTTPVERSIZE];
	assoc_t  http_heads;
	string_t http_body;
} HTTP_req;

/* HTTP response */
typedef struct {
	char     http_ver[HTTPVERSIZE];
	short    http_code;
	string_t http_codedesc;
	assoc_t  http_heads;
	string_t http_body;
} HTTP_res;


/* Return generic HTTP_req object */
HTTP_req http_generic_req(const char *path);

/* Determine length of HTTP request if it was converted to text */
size_t http_req_len(const HTTP_req *req);

/* Convert HTTP_req structure to text */
char *http_req_str(const HTTP_req *req, size_t req_len);

/* Extract HTTP headers from HTTP packet and return them as assoc_t object */
assoc_t http_extract_headers(string_t http_packet);

/* Recieve HTTP response packet and convert it to HTTP_res object */
void http_recieve_res(int sockfd, HTTP_res *res);

#include "./http.c"

#endif /* HTTP_H */
