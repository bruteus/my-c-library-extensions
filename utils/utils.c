void fatal(char *msg) {
	fprintf(stderr, "The following error occured:\n\t%s\n", msg);
	exit(-1);
}

void dump(char *msg, size_t len) {
	char line[DUMPROWLEN];
	memset(line, 0, DUMPROWLEN);

	/*
	  Dump format:
	  -> 5-digit memory offset
	  -> DUMPROWLEN space-separated char hex codes
	  -> DUMPROWLEN ASCII chars (a dot for non-printable chars)
	 */

	for(int i = 0; i <= len; i += DUMPROWLEN) {
		printf("%05d  | ", i);  // Memory offset

		if(i + DUMPROWLEN <= len)  // Check if we can copy DUMPROWLEN chars
			memcpy(line, msg + i, DUMPROWLEN);
		else {
			memcpy(line, msg + i, len - i);
			memset(line + (len - i), 0, DUMPROWLEN - (len - i));  // Fill remaining space with 0s
		}

		for(int k = 0; k < DUMPROWLEN; k += 1)
			printf(" %02X", line[k]);  // Hex codes

		printf("  |  ");

		for(int k = 0; k < DUMPROWLEN; k += 1) {
			if((int)line[k] < 32 || (int)line[k] > 126)
				putchar('.');  // Dot for unprintable chars
			else
				putchar(line[k]);
		}

		printf("  |\n");
	}
}
