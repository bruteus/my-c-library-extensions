#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DUMPROWLEN 16

void fatal(char *msg);
void dump(char *msg, size_t len);

#include "./utils.c"

#endif /* UTILS_H */

